package com.example.parser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import java.time.LocalDate;
import java.util.regex.Pattern;

@SpringBootApplication
public class ParserApplication {
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(ParserApplication.class, args);
		ContentReader contentReader = context.getBean(ContentReader.class);
		CounterData counterData = context.getBean(CounterData.class);
		PatternDate date = context.getBean(PatternDate.class);

		Pattern patternDataNews = Pattern.compile("<td class=tdate>" + LocalDate.now().format(date.formatterDate()) + "</td>");
		Pattern patternBooks = Pattern.compile("</a> " + LocalDate.now().format(date.formateDate()));

		String httpPage = contentReader.getHttpPage("https://www.opennet.ru/opennews/mini.shtml");
		String httpPage1 = contentReader.getHttpPage("https://mirknig.su/");

		long counter = counterData.counter(httpPage, patternDataNews);
		long counter1 = counterData.counter(httpPage1, patternBooks);

		System.out.println(counter + " and " + counter1);
	}
}
