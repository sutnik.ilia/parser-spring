package com.example.parser;

import org.apache.http.client.fluent.Request;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Scope("prototype")
public class ContentReaderImpl implements ContentReader {
    public String getHttpPage(String url) {
        try {
            return Request.Post(url)
                    .setHeader("User-Agent", "MySuperUserAgent")
                    .execute()
                    .returnContent()
                    .asString();
        } catch (IOException e) {
            throw new RuntimeException("Can't parse html page " + url);
        }
    }
}
