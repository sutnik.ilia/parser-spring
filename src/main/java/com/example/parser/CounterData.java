package com.example.parser;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Component
public class CounterData {
    public long counter (String page, Pattern dataRegex) {
        Matcher matcher = dataRegex.matcher(page);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }
}
