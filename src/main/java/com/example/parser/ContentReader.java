package com.example.parser;

public interface ContentReader {
    String getHttpPage(String url);
}
